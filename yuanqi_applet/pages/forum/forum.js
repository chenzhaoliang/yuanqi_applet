// pages/forums/forum.js
var util = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    forum: {},
    articles_new: [
      // {
      //   attachment:'',
      //   author:'',
      //   authorid: '',
      //   dateline:'',
      //   fid:'',
      //   head_img:'',
      //   images:[],
      //   pid:'',
      //   posttableid:'',
      //   recommend_add:'',
      //   replies:'',
      //   subject:'',
      //   tid:'',
      // }
    ],
    articles_hot: [],
    articles_reply: [],
    currentTab: 0,
    page_l_new: 1,
    page_l_hot: 1,
    page_l_reply: 1,
    isHideLoadMore1: true,
    isHideLoadMore2: true,
    isHideLoadMore3: true,
    topBarItems: [
      // id name selected 选中状态
      { id: '0', name: '最新' },
      { id: '1', name: '热帖' },
      { id: '2', name: '回帖' }
    ],
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var that = this;
    console.log('onLoad', options);
    this.setData({
      forum: JSON.parse(options.forum)
    });
    wx.setNavigationBarTitle({
      title: this.data.forum.name
    });
    this.getArticleList(true);

    wx.getSystemInfo({
      success: function(res) {
        that.setData({
          winWidth: res.windowWidth,
          winHeight: res.windowHeight
        });
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {
    // 获取组件
    this.loadMore = this.selectComponent("#loadMore");
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  //下拉刷新
  onPullDownRefresh: function() {
    wx.showNavigationBarLoading() //在标题栏中显示加载
      //模拟加载
    this.getArticleList(false);
    wx.stopPullDownRefresh(); //停止下拉刷新
  },
  //加载更多    因为只调用一次，不适合用于上拉加载更多
  onReachBottom: function() {

  },
  //列表滚动到底部的接听
  searchScrollLower: function () {
    console.log('列表滚动到底部');
    this.getArticleList(false);
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },

  getArticleList: function(refresh) {
    var that = this;
    var page_l;
    if (refresh) {
      if (this.data.currentTab == 0) {
        this.setData({
          page_l_new: 1
        });
        page_l = this.data.page_l_new;
      } else if (this.data.currentTab == 1) {
        this.setData({
          page_l_hot: 1
        });
        page_l = this.data.page_l_hot;
      } else if (this.data.currentTab == 2) {
        this.setData({
          page_l_reply: 1
        });
        page_l = this.data.page_l_reply;
      }
    } else {
      if (this.data.currentTab == 0) {
        var p1 = this.data.page_l_new;
        this.setData({
          page_l_new: p1 + 1
        });
        page_l = this.data.page_l_new;
      } else if (this.data.currentTab == 1) {
        var p2 = this.data.page_l_hot;
        this.setData({
          page_l_hot: p2 + 1
        });
        page_l = this.data.page_l_hot;
      } else if (this.data.currentTab == 2) {
        var p3 = this.data.page_l_reply;
        this.setData({
          page_l_reply: p3 + 1
        });
        page_l = this.data.page_l_reply;
      }
    }
    util.getData('/plugin.php', {
      id: 'xiaoya_appjson',
      func: 'get_forum_post',
      fid: that.data.forum.fid,
      type: this.data.currentTab + 1,
      page_l: page_l,
      page_r: 10
    }, function(res) {
      console.log('getArticleList', res.data);
      wx.hideNavigationBarLoading() //完成停止加载
      if (res.data.code == 0) {
        if (that.data.currentTab == 0) {
          if (refresh) {
            that.setData({
              articles_new: res.data.data,
              isHideLoadMore1: false
            });
          } else {
            var arr1 = that.data.articles_new;
            for (var i = 0; i < res.data.data.length; i++) {
              arr1.push(res.data.data[i]);
            }
            that.setData({
              articles_new: arr1
            });
            if (res.data.data.length == 0) {
              that.setData({
                isHideLoadMore1: true
              })
            }
          }
        } else if (that.data.currentTab == 1) {
          if (refresh) {
            that.setData({
              articles_hot: res.data.data,
              isHideLoadMore2: false
            });
          } else {
            var arr2 = that.data.articles_hot;
            for (var i = 0; i < res.data.data.length; i++) {
              arr2.push(res.data.data[i]);
            }
            that.setData({
              articles_hot: arr2
            })
            if (res.data.data.length == 0) {
              that.setData({
                isHideLoadMore2: true
              })
            }
          }
        } else if (that.data.currentTab == 2) {
          if (refresh) {
            that.setData({
              articles_reply: res.data.data,
              isHideLoadMore3: false
            });
          } else {
            var arr3 = that.data.articles_reply;
            for (var i = 0; i < res.data.data.length; i++) {
              arr3.push(res.data.data[i]);
            }
            that.setData({
              articles_reply: arr3
            })
            if (res.data.data.length == 0) {
              that.setData({
                isHideLoadMore3: true
              })
            }
          }
        }
      }
    })
  },
  /** 
   * 滑动切换tab 
   */
  bindChange: function(e) {
    var that = this;
    that.setData({ currentTab: e.detail.current });
    if (that.data.currentTab == 0 && that.data.articles_new.length == 0) {
      that.getArticleList(true);
    } else if (that.data.currentTab == 1 && that.data.articles_hot.length == 0) {
      that.getArticleList(true);
    } else if (that.data.currentTab == 2 && that.data.articles_reply.length == 0) {
      that.getArticleList(true);
    }
  },
  /** 
   * 点击tab切换 
   */
  swichNav: function(e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setData({
        currentTab: e.target.dataset.current
      });
      if (that.data.currentTab == 0 && that.data.articles_new.length == 0) {
        that.getArticleList(true);
      } else if (that.data.currentTab == 1 && that.data.articles_hot.length == 0) {
        that.getArticleList(true);
      } else if (that.data.currentTab == 2 && that.data.articles_reply.length == 0) {
        that.getArticleList(true);
      }
    }
  },
  tapArticle: function(event) {
    console.log(event);
    var tid = event.currentTarget.dataset.id;
    wx.navigateTo({
      url: '../article/article?tid=' + tid
    })
  }
})