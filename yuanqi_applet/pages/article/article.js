 var WxParse = require('../../wxParse/wxParse.js');

Page({
  message: [{
    name: 'images',
    attrs: {
      class: 'div_class',
      style: 'width : 30px; height : 100px; color: red;'
    },

  }],


  /**
   * 页面的初始数据
   */
  data: {
  
    txt:'',
    article: {},
    user: {},
    replys: [
      // {
      //   author: '',
      //   authorid: '',
      //   message: '',
      //   dateline: '',
      //   avatar: '',
      //   pid: ''
      // }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    this.setData({
      article: {tid: options.tid}
    }); 
    wx.setNavigationBarTitle({
      title: ''
    });
    wx.getStorage({
      key: 'user',
      success: function(res) {
        that.setData({
          user: res.data
        })
      },
    });
    this.getArticleDetail();
    this.getArticleReplys();
  },


  /**
   * 监听输入
   */
  txt: function (e) {
    this.data.txt = e.detail.value;
  },

  clickComment: function(e) {
    var that = this;
    if (typeof (this.data.user.uid) == 'undefined') {
      wx.showToast({
        title: '只有登录用户才能回复帖子!',
        icon: 'loading',
        duration: 2000
      })
    } else {
      if (this.data.txt == "") {
        wx.showToast({
          title: '回帖内容不得为空!',
          icon: 'loading',
          duration: 2000
        })
      } else {
        wx.request({
          url: 'https://www.xmyuanqi.cn/plugin.php',
          header: {
            "Content-Type": "application/x-www-form-urlencoded"
          },
          method: "POST",
          data: {
            uid: that.data.user.uid,
            tid: that.data.article.tid,
            fid: '',
            subject: '',
            message: that.data.txt
          },
          success: function (res) {
            if (res.data.code == 0) {
              wx.showToast({
                title: '成功',
                icon: 'success',
                duration: 1500
              })
            } else {
              wx.showToast({
                title: '失败',
                icon: 'loading',
                duration: 1500,
              })
            }
          }
        })
      }
    }
  },

  clickGood: function (e) {
    if (typeof (this.data.user.uid) == 'undefined') {
      wx.showToast({
        title: '只有登录用户才能点赞!',
        icon: 'loading',
        duration: 2000
      })
    } else {

    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },

  getArticleDetail: function() {
    var that = this;
    console.log('文章id=' + this.data.article.tid);
    wx.request({
      url: 'https://www.xmyuanqi.cn/plugin.php?id=xiaoya_appjson&func=get_forum_post_a&tid=' + this.data.article.tid + (typeof (this.data.user.uid) !='undefined' ? '&uid=' + this.data.user.uid : ''),
      data: {

      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        console.log('文章详情'+res);
        that.setData({
          article: res.data.data[0],
          wxParseData: WxParse.wxParse('msme', 'html', res.data.data[0].message, that, 5)
        });
        wx.setNavigationBarTitle({
          title: res.data.data[0].subject//页面标题为路由参数
        })
      },
    })
  },

  getArticleReplys: function() {
    var that = this;
    wx.request({
      url: 'http://www.xmyuanqi.cn/plugin.php?id=xiaoya_appjson&func=get_first_comment&tid=' + this.data.article.tid + (typeof (this.data.user.uid) != 'undefined' ? '&uid=' + this.data.user.uid : ''),
      data: {

      },
      header: {
        'content-type': 'application/json' // 默认值
      },
      success: function (res) {
        if (res.data.code == 0) {
          console.log(res.data.data, 300);
     //console.log(res.data.data[1], 200);
        

    
 






      

             that.setData({
         replys: res.data.data,

     
         

    
           });

       //wxParseData: WxParse.wxParse('msme', 'html', res.data.data[i].message, that, 5)
          
    
       









      
    

        }
      },
    })
  },

  onComment: function() {
    console.log('评论');
  },
  
  onGood: function () {
    console.log('点赞');
  }
})