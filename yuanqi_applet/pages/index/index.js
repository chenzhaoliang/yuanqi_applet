//index.js
//获取应用实例
var util = require('../../utils/util.js');

Page({
  data: {
    advertice: [],
    forums: [{
      banner: '',
      description: '详细介绍',
      fid: 'fid',
      icon: '',
      name: '',
      todayposts: ''
    }],
    newEvents: [],
    user: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo')
  },
  onReady: function() {
    //this.getOpenid();
    this.getAdvertice();
    this.getForums();
  },
  //事件处理函数
  //首页轮播图的事件处理
  intervalChange: function(e) {
    this.setData({
      interval: e.detail.value
    })
  },
  getUserInfo: function(e) {
    console.log('getUserInfo', e)
    app.globalData.userInfo = e.detail.userInfo
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  //   getOpenid: function() {
  //     //调用微信登录接口  
  //     wx.login({
  //       success: function(loginCode) {
  //         //调用request请求api转换登录凭证  
  //         wx.request({
  //           url: 'https://api.weixin.qq.com/sns/jscode2session?appid=wx35520a9fa9f69970&secret=4f5fac372991b3baec70510241e49b89&grant_type=authorization_code&js_code=' + loginCode.code,
  //           header: {
  //             'content-type': 'application/json'
  //           },
  //           success: function(res) {
  //             console.log('openid=' + res.data.openid) //获取openid  
  //           }
  //         })
  //       }
  //     }) plugin.php?xiaoya_appjson=get_banner_recycle
  //   },
  getAdvertice: function() {
    var that = this;
    util.getData('/plugin.php', {
      id: 'xiaoya_appjson',
      func: 'get_banner_recycle'
    }, function(res) {
      console.log('getAdvertice', res.data);
      var forum_url = 'http://www.xmyuanqi.cn/data/attachment/forum/';
      if (res.data.code == 0) {
        for (var i = 0; i < res.data.data.length; i++) {
          res.data.data[i].attachment = forum_url + res.data.data[i].attachment;
        }
        that.setData({
          advertice: res.data.data
        });
      }
    })
  },
  getForums: function() {
    var that = this;
    util.getData('/plugin.php', {
      id: 'xiaoya_appjson',
      func: 'get_forum_forum'
    }, function(res) {
      console.log('getForums', res.data);
      if (res.data.code == 0) {
        that.setData({
          forums: res.data.data
        });


        console.log(res.data.data, 100);
      }
    })
  },
  tapFourm: function(event) {
    var id = event.currentTarget.dataset.id;
    var fourm = this.data.forums[parseInt(id)];
    console.log('tapFourm', id, fourm);
    wx.navigateTo({
        
     // url:'../article/article?tid=8783'  
       url: '../forum/forum?forum=' + JSON.stringify(fourm)
    })
  }
})